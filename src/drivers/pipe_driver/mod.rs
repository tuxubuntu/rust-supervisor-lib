
mod instance;

use std::process::Child;
use std::collections::HashMap;

use self::instance::SimpleInstance;


use drivers::Instance;
use drivers::Driver;
use Error;
use Result;
use DriverID;
use ModuleID;



pub struct SimpleDriver {
	id: DriverID,
	modules: HashMap<ModuleID, (String, Vec<String>)>,
}

impl From<DriverID> for SimpleDriver {
	fn from(id: DriverID) -> SimpleDriver {
		SimpleDriver {
			id,
			modules: HashMap::new(),
		}
	}
}


impl SimpleDriver {
	pub fn add_module(&mut self, mid: ModuleID, cmd: String, args: Vec<String>) -> Result<()> {
		if self.contain_module(&mid) {
			return Err(Error::from(format!("Module already registred in driver {:?}", self.id)))
		}
		self.modules.insert(mid, (cmd, args));
		Ok(())
	}
	pub fn del_module(&mut self, mid: &ModuleID) -> Result<()> {
		match self.modules.remove(mid) {
			Some(_) => Ok(()),
			None => Err(Error::from(format!("Module not found in driver {:?}", self.id))),
		}
	}
	pub fn contain_module(&self, mid: &ModuleID) -> bool {
		self.modules.contains_key(mid)
	}
}


impl Driver for SimpleDriver {
	fn id(&self) -> DriverID {
		self.id.clone()
	}
	fn modules(&self) -> Vec<ModuleID> {
		self.modules.keys().map(|&x| x).collect()
	}
	fn init(&mut self) -> Result<()> {
		Ok(())
	}
	fn up_instance(&mut self, mid: ModuleID) -> Result<Box<Instance>> {
		let (name, args) = self.modules.get(&mid)
			.ok_or("Module not found.")?;
		let child = exec(name.clone(), args.clone())?;
		Ok(Box::new(SimpleInstance::from(child)))
	}
}


fn exec(name: String, args: Vec<String>) -> Result<Child> {
	use std::process::Stdio;
	use std::process::Command;
	let child = Command::new(name)
		.args(args)
		.stdin(Stdio::piped())
		.stdout(Stdio::piped())
		.spawn()?;
	Ok(child)
}
