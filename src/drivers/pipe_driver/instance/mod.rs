
mod reader;
mod writer;

pub use self::reader::SimpleReader;
pub use self::writer::SimpleWriter;

use Result;
use drivers::Instance;
use drivers::Reader;
use drivers::Writer;

use std::process::Child;


pub struct SimpleInstance(Option<Child>);

impl From<Child> for SimpleInstance {
	fn from(child: Child) -> SimpleInstance {
		SimpleInstance(Some(child))
	}
}

impl Instance for SimpleInstance {
	fn split(&mut self) -> Result<(Box<Reader>, Box<Writer>)> {
		let mut child = self.0.take()
			.ok_or("None child process")?;
		let stdin = child.stdin.take()
			.ok_or("None stdin in split")?;
		let stdout = child.stdout.take()
			.ok_or("None stdout in split")?;
		let writer = SimpleWriter::from(stdin);
		let reader = SimpleReader::from(stdout);
		self.0 = Some(child);
		Ok((Box::new(reader), Box::new(writer)))
	}
	fn down(&mut self) -> Result<()> {
		let mut child = self.0.take()
			.ok_or("None child process")?;
		child.kill()?;
		Ok(())
	}
}
