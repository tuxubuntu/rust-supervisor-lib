use std::process::ChildStdout;

use Data;
use Result;
use drivers::Reader;

pub struct SimpleReader(ChildStdout);

impl Reader for SimpleReader {
	fn read(&mut self, len: usize) -> Result<Data> {
		use std::io::Read;
		let mut buf = vec![0u8; len];
		self.0.read_exact(&mut buf)?;
		Ok(buf)
	}
}

impl From<ChildStdout> for SimpleReader {
	fn from(stdout: ChildStdout) -> SimpleReader {
		SimpleReader(stdout)
	}
}

