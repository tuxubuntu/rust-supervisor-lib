use std::process::ChildStdin;

use Data;
use Result;
use drivers::Writer;

pub struct SimpleWriter(ChildStdin);

impl From<ChildStdin> for SimpleWriter {
	fn from(stdin: ChildStdin) -> SimpleWriter {
		SimpleWriter(stdin)
	}
}

impl Writer for SimpleWriter {
	fn write(&mut self, data: &Data) -> Result<()> {
		use std::io::Write;
		self.0.write_all(data)?;
		Ok(())
	}
}
