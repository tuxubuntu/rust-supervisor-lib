pub mod pipe_driver;

use Data;
use Result;
use DriverID;
use ModuleID;

/// Reader for Instance
pub trait Reader where Self: Send {
	/// Read `size` bytes from Instance.
	fn read(&mut self, size: usize) -> Result<Data>;
}

/// Writer for Instance
pub trait Writer where Self: Send {
	/// Write data to Instance.
	fn write(&mut self, data: &Data) -> Result<()>;
}

/// Instance of module
pub trait Instance where Self: Send {
	/// Split Instance to Reader and Writer.
	fn split(&mut self) -> Result<(Box<Reader>, Box<Writer>)>;
	/// Close Instance process.
	fn down(&mut self) -> Result<()>;
}

/// Driver for work with modules.
pub trait Driver where Self: Send {
	/// Return name (DriverID) of driver.
	fn id(&self) -> DriverID;
	/// Initialize driver.
	fn init(&mut self) -> Result<()>;
	/// List of registred modules.
	fn modules(&self) -> Vec<ModuleID>;
	/// Startup instance by `module_id`.
	fn up_instance(&mut self, module_id: ModuleID) -> Result<Box<Instance>>;
}

