
/// Error object
#[derive(Debug)]
pub struct Error(String);

use std::io;
impl From<io::Error> for Error {
	fn from(err: io::Error) -> Error {
		Error(format!("{}", err))
	}
}

impl<'a> From<&'a str> for Error {
	fn from(err: &'a str) -> Error {
		Error(format!("{}", err))
	}
}

use std::sync::mpsc::RecvError;
impl From<RecvError> for Error {
	fn from(err: RecvError) -> Error {
		Error(format!("{}", err))
	}
}

impl From<String> for Error {
	fn from(err: String) -> Error {
		Error(err)
	}
}

use std::fmt::Debug;
use std::sync::mpsc::SendError;
impl<T> From<SendError<T>> for Error where T: Debug {
	fn from(err: SendError<T>) -> Error {
		Error(format!("{}", err))
	}
}
