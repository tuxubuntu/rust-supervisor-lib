
use Core;
use ModuleID;
use drivers::pipe_driver::SimpleDriver;



const MID_CAT: ModuleID = [
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
];

const MID_DOCKER_CAT: ModuleID = [
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 1,
];

#[test]
fn it_works() {
	use std::thread;
	use std::time;
	let mut core = Core::new();
	let mut driver = SimpleDriver::from(String::from("test_driver"));
	driver.add_module(MID_CAT, String::from("cat"), vec![]).unwrap();
	driver.add_module(MID_DOCKER_CAT,
		String::from("docker"),
		vec![ String::from("run"), String::from("--rm"),
			String::from("-i"), String::from("alpine"), String::from("cat") ]
	).unwrap();
	driver.del_module(&MID_DOCKER_CAT).unwrap();
	core.reg_driver(driver).unwrap();
	let mut lock = core.init().unwrap();
	let receiver = lock.receiver().unwrap();
	let mlist = core.modules();
	let mid = mlist[0];
	let sid = core.init_session(mid, true).unwrap();
	let iid = core.target_id(&sid).unwrap();
	core.send(iid, b"lalka".to_vec()).unwrap();
	let mut i = 0;
	thread::spawn(move || {
		for msg in receiver.iter() {
			println!("\t>>{:?}", msg);
			thread::sleep(time::Duration::from_millis(10));
			core.send(msg.0, msg.1).unwrap();
			if i > 10 {
				break;
			} else {
				i += 1;
			}
		}
		lock.wait().unwrap();
		core.close_session(sid).unwrap();
	});
	thread::sleep(time::Duration::from_millis(1000));
}
