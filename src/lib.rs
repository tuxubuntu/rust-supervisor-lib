extern crate containerizer;

extern crate rand;

/// Drivers for work with modules of supervisor.
pub mod drivers;

mod core;

pub use core::Core;
pub use core::locker;

mod error;

pub use error::Error;

type Data = Vec<u8>;
type Result<T> = std::result::Result<T, Error>;

/// Identificator for Driver.
/// Used as name of driver.
pub type DriverID = String;

/// Identificator for Module
/// Used as name of module.
pub type ModuleID = [u8; 16];

/// Identificator for Session
/// Used as session token
pub type SessionID = [u8; 32];



#[cfg(test)]
mod tests;
