

use Data;
use Error;
use Result;
use drivers::Reader;


// use message;
use containerizer::{
	Container,
	raw::{ RawLen32, RawLen64 }
};

pub fn pack(data: Data) -> Result<Data> {
	use std;
	if data.len() < std::u32::MAX as usize {
		let data = RawLen32::encode(data)?;
		Ok(data)
	} else {
		let data = RawLen64::encode(data)?;
		Ok(data)
	}
}


pub fn read_msg(mut reader: &mut Box<Reader>) -> Result<Data> {
	let mut state = State::Init;
	loop {
		state = read(&mut reader, state)?;
		if let State::Data(data) = state {
			break Ok(data)
		}
	}
}


#[derive(Debug)]
pub enum State {
	Init,
	Code(u8),
	Len(usize),
	Data(Vec<u8>),
}


fn read(stream: &mut Box<Reader>, state: State) -> Result<State> {
	match state {
		State::Init => {
			let buf = stream.read(1)?;
			Ok(State::Code(buf[0]))
		},
		State::Code(code) => {
			if code == RawLen32::code() {
				let buf = stream.read(4)?;
				Ok(State::Len(RawLen32::len(&buf)))
			} else if code == RawLen64::code() {
				let buf = stream.read(8)?;
				Ok(State::Len(RawLen64::len(&buf)))
			} else {
				Err(Error::from("Wrong Code"))
			}
		},
		State::Len(len) => {
			let buf = stream.read(len)?;
			Ok(State::Data(buf))
		},
		State::Data(data) => Ok(State::Data(data)),
	}
}
