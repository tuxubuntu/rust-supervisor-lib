use Result;
use Error;
use super::Message;
use std::sync::mpsc;

pub struct Locker(Option<mpsc::Receiver<()>>, Option<mpsc::Receiver<Message>>);
pub struct Unlocker(mpsc::Sender<()>);

impl From<mpsc::Receiver<Message>> for Locker {
	fn from(receiver: mpsc::Receiver<Message>) -> Locker {
		Locker(None, Some(receiver))
	}
}

impl Locker {
	pub fn wait(self) -> Result<()> {
		self.0
			.ok_or(Error::from("None unlocker."))?
			.recv()?;
		Ok(())
	}
	pub fn unlocker(&mut self) -> Result<Unlocker> {
		if self.0.is_none() {
			let (sender, receiver) = mpsc::channel();
			self.0 = Some(receiver);
			Ok(Unlocker(sender))
		} else {
			Err(Error::from("Already has unlocker."))
		}
	}
	pub fn receiver(&mut self) -> Result<mpsc::Receiver<Message>> {
		let receiver = self.1.take()
			.ok_or(Error::from("Messages receiver already used."))?;
		Ok(receiver)
	}
}

impl Unlocker {
	pub fn unlock(&mut self) -> Result<()> {
		self.0.send(())?;
		Ok(())
	}
}
