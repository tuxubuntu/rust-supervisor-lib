
mod utils;
mod session;

/// Lock thread while Supervisor is works.
pub mod locker;

use self::locker::Locker;
use self::session::Session;

use std::collections::HashMap;
use std::thread;
use std::sync::{ mpsc, Arc, Mutex };


use Data;
use Result;
use SessionID;
use DriverID;
use ModuleID;
use drivers::Driver;
use drivers::Instance;
use drivers::Writer;
use Error;

type InstanceID = usize;
type Message = (InstanceID, Data);

/// Supervisor implementation
pub struct Core {
	// modules
	modules: HashMap<ModuleID, DriverID>,
	public: HashMap<ModuleID, InstanceID>,
	// drivers
	drivers: HashMap<DriverID, Box<Driver>>,
	writer_sender: Option<mpsc::Sender<(InstanceID, Data)>>,
	message_sender: Option<mpsc::Sender<Message>>,
	// instances
	instances: HashMap<InstanceID, ModuleID>,
	writers: Arc<Mutex<HashMap<InstanceID, Box<Writer>>>>,
	closers: Arc<Mutex<HashMap<InstanceID, Box<Instance>>>>,
	// sessions
	sobjs: HashMap<SessionID, Session>,
	sessions: HashMap<InstanceID, Vec<SessionID>>,
}


impl Core {
	pub fn new() -> Core {
		Core {
			// modules
			modules: HashMap::new(),
			public: HashMap::new(),
			// drivers
			drivers: HashMap::new(),
			writer_sender: None,
			message_sender: None,
			// instances
			instances: HashMap::new(),
			writers: Arc::new(Mutex::new(HashMap::new())),
			closers: Arc::new(Mutex::new(HashMap::new())),
			// sessions
			sessions: HashMap::new(),
			sobjs: HashMap::new(),
		}
	}
	pub fn init(&mut self) -> Result<Locker> {
		let mut modules = HashMap::new();
		for (did, driver) in self.drivers.iter_mut() {
			driver.init()?;
			for mid in driver.modules() {
				modules.insert(mid, did.clone());
			}
		}
		self.modules = modules;
		let (sender, receiver) = mpsc::channel();
		self.message_sender = Some(sender);
		let mut locker = Locker::from(receiver);
		let mut unlocker = locker.unlocker()?;
		let (sender, receiver) = mpsc::channel();
		self.writer_sender = Some(sender);
		let writers = self.writers.clone();
		let builder = thread::Builder::new()
			.name("Write channel".into());
		builder.spawn(move || {
			for (id, data) in receiver.iter() {
				let mut writers = writers.lock().unwrap();
				if let Some(ref mut writer) = writers.get_mut(&id) {
					writer.write(&data).unwrap();
				}
			}
			unlocker.unlock().unwrap();
		}).unwrap();
		Ok(locker)
	}
	pub fn reg_driver(&mut self, driver: impl Driver + 'static) -> Result<DriverID> {
		let did = driver.id();
		if self.drivers.contains_key(&did) {
			use Error;
			Err(Error::from("Driver already registred."))
		} else {
			self.drivers.insert(did.clone(), Box::new(driver));
			Ok(did)
		}
	}
}

impl Core {
	pub fn modules(&self) -> Vec<ModuleID> {
		self.modules.keys().map(|&x| x).collect()
	}
	pub fn reg_module(&mut self, did: DriverID, mid: ModuleID) -> Result<ModuleID> {
		if self.modules.contains_key(&mid) {
			Err(Error::from("Module already registred."))
		} else {
			self.modules.insert(mid.clone(), did);
			Ok(mid)
		}
	}
	pub fn reverce_session(&mut self, sid: SessionID) -> Result<SessionID> {
		let target = {
			let session = self.sobjs.get(&sid)
				.ok_or("Session not found")?;
			session.source.clone()
				.ok_or("Session don't have a source")?
		};
		let source = {
			let session = self.sobjs.get(&sid)
				.ok_or("Session not found")?;
			session.target.clone()
		};
		let sid = self.reg_session(Some(source), target)?;
		Ok(sid)
	}
	pub fn share_session(&mut self, sid: SessionID, tgt_sid: SessionID) -> Result<SessionID> {
		let source = {
			let session = self.sobjs.get(&sid)
				.ok_or("Session not found")?;
			session.target.clone()
		};
		let target = {
			let session = self.sobjs.get(&tgt_sid)
				.ok_or("Session not found")?;
			session.target.clone()
		};
		let sid = self.reg_session(Some(source), target)?;
		Ok(sid)
	}
	pub fn init_session(&mut self, mid: ModuleID, private: bool) -> Result<SessionID> {
		self.open_session(None, mid, private)
	}
	pub fn open_session(&mut self, source: Option<InstanceID>, mid: ModuleID, private: bool) -> Result<SessionID> {
		let target = if private {
			self.up_instance(&mid)?
		} else {
			if self.public.contains_key(&mid) {
				self.public.get(&mid).cloned().unwrap()
			} else {
				let target = self.up_instance(&mid)?;
				self.public.insert(mid, target.clone());
				target
			}
		};
		let sid = self.reg_session(source, target)?;
		Ok(sid)
	}
	pub fn close_session(&mut self, sid: SessionID) -> Result<()> {
		let is_public = self.is_public(&sid)
			.ok_or("Session not found")?;
		let session = self.sobjs.get(&sid)
			.ok_or("Session not found")?;
		let sessions = self.sessions.get_mut(&session.target)
			.ok_or("Session not found")?;
		let id = session.id.clone();
		let index = sessions.iter().position(|&r| r == id).unwrap();
		sessions.remove(index);
		let down = sessions.len() == 0;
		if !is_public && down {
			self.public.remove(&session.module);
		}
		if down {
			let mut writers = self.writers.lock().unwrap();
			writers.remove(&session.target).unwrap();
			let mut closers = self.closers.lock().unwrap();
			let mut closer = closers.remove(&session.target).unwrap();
			closer.down()?;
		}
		Ok(())
	}
	pub fn send(&mut self, target: InstanceID, data: Data) -> Result<()> {
		let sender = self.writer_sender.clone().unwrap();
		let data = utils::pack(data)?;
		sender.send((target, data))?;
		Ok(())
	}
	pub fn broadcast(&mut self, sid: SessionID, data: Data) -> Result<()> {
		let sessions = {
			let session = self.sobjs.get(&sid)
				.ok_or("Session not found")?;
			let source = session.source
				.ok_or("Session don't have a source")?;
			let sessions = self.sessions.get(&source)
				.ok_or("Session not found")?;
			sessions.clone()
		};
		for sid in sessions {
			let iid = self.target_id(&sid).unwrap();
			self.send(iid, data.clone())?;
		}
		Ok(())
	}
	pub fn target_id(&self, sid: &SessionID) -> Option<InstanceID> {
		if let Some(session) = self.sobjs.get(sid) {
			Some(session.target)
		} else {
			None
		}
	}
}

impl Core {
	fn reg_session(&mut self, source: Option<InstanceID>, target: InstanceID) -> Result<SessionID> {
		let mid = self.instances.get(&target)
			.ok_or("Instance not found")?
			.clone();
		let session = Session::from((source, target.clone(), mid));
		let sid = session.id.clone();
		self.sobjs.insert(session.id, session);
		let ref mut sessions = self.sessions.get_mut(&target).unwrap();
		sessions.push(sid.clone());
		Ok(sid)
	}
	fn up_instance(&mut self, mid: &ModuleID) -> Result<InstanceID> {
		let mut instance = {
			let driver = self.get_driver_by_mid(mid)?;
			driver.up_instance(mid.clone())?
		};
		let (mut reader, writer) = instance.split()?;
		let iid = self.reg_instance(instance, writer);
		self.instances.insert(iid.clone(), mid.clone());
		let iid_in = iid.clone();
		let writers = self.writers.clone();
		let message_sender = self.message_sender.clone().unwrap();
		let builder = thread::Builder::new()
			.name((format!("Instance reader #{}", iid)).into());
		builder.spawn(move || {
			loop {
				match utils::read_msg(&mut reader) {
					Ok(data) => {
						message_sender.send((iid_in.clone(), data)).unwrap();
					},
					Err(err) => {
						let writers = writers.lock().unwrap();
						if writers.contains_key(&iid_in) {
							panic!(err);
						} else {
							println!("Instance #{:?} is down.", iid_in);
							break;
						}
					},
				}
			}
		}).unwrap();
		Ok(iid)
	}
	fn reg_instance(&mut self, instance: Box<Instance>, writer: Box<Writer>) -> InstanceID {
		let iid = self.gen_iid();
		let mut writers = self.writers.lock().unwrap();
		writers.insert(iid.clone(), writer);
		let mut closers = self.closers.lock().unwrap();
		closers.insert(iid.clone(), instance);
		self.sessions.insert(iid.clone(), Vec::new());
		iid
	}
	fn gen_iid(&self) -> InstanceID {
		use rand;
		loop {
			let iid = rand::random::<InstanceID>();
			if !self.sessions.contains_key(&iid) {
				break iid
			}
		}
	}
	fn get_driver(&mut self, did: &DriverID) -> Result<&mut Box<Driver>> {
		let driver = self.drivers.get_mut(did)
			.ok_or("Driver not found")?;
		Ok(driver)
	}
	fn get_driver_by_mid(&mut self, mid: &ModuleID) -> Result<&mut Box<Driver>> {
		let did = self.modules.get(mid)
			.ok_or("Module not found")?
			.clone();
		self.get_driver(&did)
	}
	fn is_public(&self, sid: &SessionID) -> Option<bool> {
		if !self.sobjs.contains_key(sid) {
			return None;
		}
		let session = self.sobjs.get(sid).unwrap();
		if !self.public.contains_key(&session.module) {
			return None;
		}
		let iid = self.public.get(&session.module).unwrap();
		Some(iid == &session.target)
	}
}
