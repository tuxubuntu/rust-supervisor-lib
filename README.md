# Docs

## API of `Core`


`fn new() -> Core;`

Make new `Core` object.

---

`fn init(&mut self) -> Result<Locker>;`

Init Core:

* up writer thread.
* init drivers.
* register driver modules.

---

`fn reg_driver(&mut self, driver: impl Driver + 'static) -> Result<DriverID>;`

Add new driver. **Allowed only when core isn't inited.**

---

`fn modules(&self) -> Vec<ModuleID>;`

Return all registred ModuleID's.

---

`fn reg_module(&mut self, did: DriverID, mid: ModuleID) -> Result<ModuleID>;`

Register new module of driver.

---

`fn reverce_session(&mut self, sid: SessionID) -> Result<SessionID>;`

Create session in different direction (from target to source).

---

`fn share_session(&mut self, sid: SessionID, tgt_sid: SessionID) -> Result<SessionID>;`

Create new session from `sid` (source) to `tgt_sid` (target).

---

`fn open_session(&mut self, sid: Option<SessionID>, mid: ModuleID, private: bool) -> Result<SessionID>;`

Open session to the Module with `mid`.
First argument may contain session of source instance.

---

`fn close_session(&mut self, sid: SessionID) -> Result<()>;`

Close session.

---

`fn send(&mut self, sid: SessionID, data: Data) -> Result<()>;`

Send data for target of session.

---

`fn broadcast(&mut self, sid: SessionID, data: Data) -> Result<()>;`

Send data from source of session to all opened sessions.

---


